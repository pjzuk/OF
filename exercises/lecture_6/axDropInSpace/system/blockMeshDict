/*--------------------------------*- C++ -*----------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Website:  https://openfoam.org
    \\  /    A nd           | Version:  6
     \\/     M anipulation  |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    object      blockMeshDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

backgroundMesh
{
    length  25;
    nozzleLength -2;
    rA     0.5; // radius of inlet patch
    rB       8; // outer radius
    lengthCells 250;
    nozzleCells 20;
    rAcells 10;
    rBcells 100;
}

convertToMeters 0.001;

vertices
(
    (                      0                   0 -1)
    ($:backgroundMesh.length                   0 -1)
    (                      0 $:backgroundMesh.rA -1)
    ($:backgroundMesh.length $:backgroundMesh.rA -1)
    (                      0 $:backgroundMesh.rB -1)
    ($:backgroundMesh.length $:backgroundMesh.rB -1)

    (                      0                   0  0)
    ($:backgroundMesh.length                   0  0)
    (                      0 $:backgroundMesh.rA  0)
    ($:backgroundMesh.length $:backgroundMesh.rA  0)
    (                      0 $:backgroundMesh.rB  0)
    ($:backgroundMesh.length $:backgroundMesh.rB  0)


    ($:backgroundMesh.nozzleLength 0 -1)
    ($:backgroundMesh.nozzleLength $:backgroundMesh.rA -1)
    ($:backgroundMesh.nozzleLength 0  0)
    ($:backgroundMesh.nozzleLength $:backgroundMesh.rA  0)

);

blocks
(
    hex (0 1 3 2 6 7 9 8)
    ($:backgroundMesh.lengthCells $:backgroundMesh.rAcells 1)
    simpleGrading (1 1 1)

    hex (2 3 5 4 8 9 11 10)
    ($:backgroundMesh.lengthCells $:backgroundMesh.rBcells 1)
    simpleGrading (1 1 1)

    hex (12 0 2 13 14 6 8 15)
    ($:backgroundMesh.nozzleCells $:backgroundMesh.rAcells 1)
    simpleGrading (1 1 1)

);

edges
(
);

boundary
(
    inlet
    {
        type patch;
        faces
        (
            (13 12 14 15)
        );
    }

    front
    {
        type symmetry;
        faces
        (
            (6 7  9 8)
            (8 9 11 10)
            (14 6 8 15)
        );
    }

    back
    {
        type symmetry;
        faces
        (
            (0 1 3 2)
            (2 3 5 4)
            (12 13 2 0)
        );
    }

    nozzle
    {
        type wall;
        faces
        (
            (2 13 15 8)
        );
    }

    walls
    {
        type wall;
        faces
        (
            (5 3  9 11)
            (3 1  7  9)
        );
    }

    atmosphere
    {
        type patch;
        faces
        (
            (2 8 10  4)
            (4 5 11 10)
        );
    }
);

mergePatchPairs
(
);

// ************************************************************************* //
